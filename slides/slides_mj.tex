\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{pgf}
\usepackage{tikz}
\usepackage{tikz-qtree}
\usepackage[sc]{mathpazo}
\usepackage[nice]{nicefrac}
\usepackage{epstopdf}
\usepackage{graphicx}
\usepackage[round]{natbib}
\usepackage{pstricks}
\usepackage[greek,english]{babel}
\usepackage{textgreek}

\usetheme{Singapore}

\makeatletter
\renewcommand\verbatim@font{\normalfont\fontencoding{T1}\ttfamily}
\makeatother

\begin{document}
\title{An efficient implementation of the Kalman filter in Julia}
\subtitle{eMAF2020 Mathematical and Statistical Methods for Actuarial
  Sciences and Finance}
\date{Remote conference, Ca' Fascari University of Venice, September 18, 2020}
\author{Michel Juillard\thanks{Banque de France, michel.juillard@mjui.fr}}

\begin{frame}
  \titlepage
{\footnotesize The views expressed herein are ours and do not
necessarily represent the views of Bank of France.}
\end{frame}

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item Estimation of unobservable components models and linearized
    DSGE models: log likelihood computed with Kalman filter
  \item Called repeatedly during estimation
  \item Most time consuming single algorithm
  \item Julia provides elegant ways to implement optimized code
  \item What is the scope for parallel computing?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Kalman filter}
  State space model:
  \begin{align*}
    y_t &= Z \alpha_t + \epsilon_t,\;\;\;\epsilon_t \sim N(0, H)\\
    \alpha_{t+1} &= T\alpha_t + R \eta_t,\;\;\;\eta_t \sim N(0,Q)\\
    \alpha_1 &\sim N(a_1, P_1)
  \end{align*}
  Kalman filter recursion
  \begin{align*}
    \nu_t &= y_t - Za_t\\
    F_t &= Z P_{t|t-1} Z' + H\\
    a_{t+1|t} &= T \left(a_{t|t-1} + P_t Z' F_t^{-1}\nu_t\right)\\
    P_{t+1|t} &= T\left(P_{t|t-1} - P_{t|t-1}Z'F_t^{-1}ZP_{t|t-1}\right)T' + R Q R'
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Optimization strategy}
  \begin{itemize}
  \item In place computations
  \item Factorizing: never compute the same thing twice even if it
    occupies memory
  \item Avoid temporary matrix allocation (cf. $A\cdot B \cdot C$
    needs $T1 = B\cdot C, T2 = A\cdot T1$) 
  \item BLAS and LAPACK are your friends
    \begin{itemize}
    \item Matrix multiplication in place, $AB + C \rightarrow C$:
      mul!() [new in Julia v. 1.3]
    \item Solving linear system with positive definite matrix using
      Choleksy decomposition: potrf!() and potrs!()
    \item Exploit multiple dispatch
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Matrix multiplication in place}
  \begin{itemize}
  \item $\alpha AB + \beta C - > C$:
\begin{verbatim}
     mul!(C, A, B, alpha, beta)   
\end{verbatim}
  \item Examples
    \begin{itemize}
    \item  $AB+0.5 C \rightarrow C$
\begin{verbatim}
     mul!(C, A, B, 1.0, 0.5)   
\end{verbatim}
    \item $2AB'+0.5C \rightarrow C$
\begin{verbatim}
     mul!(C, A, B', 2, 0.5)   
\end{verbatim}
    \item $2A'B' \rightarrow C$
\begin{verbatim}
     mul!(C, A', B', 2.0, 0.0)   
\end{verbatim}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Cholesky   decomposition}
  \begin{itemize}
  \item F is a symmetric matrix
  \item Cholesky decomposition: triangular matrix $U$ such that $U'U =
    F$ (for \verb+uplo='U'+)
  \item \verb+LAPACK.potrf!(uplo, F)+ returns upper/lower triangular
    matrix in $F$  
  \item Solution of $FX=A$: solves two triangular systems: $U'Y = A$,
    $UX = Y$
  \item \verb+LAPACK.potrs!(uplo, F, A)+ returns the solution of
    $FX=A$ using the Choleski decomposition of $F$ returned by
    \verb+LAPACK.potrf!()+
  \item The determinant of matrix $F$ is
    \[
      \left|F\right| = \sum_{i=1,\ldots,n} U_{i,i}^2
    \]
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Kalman  filter recursion in Julia (I)}
  \begin{enumerate}
  \item $\nu_t = y_t - Za_{t|t-1}$
\begin{verbatim}
       copy!(ν, y)
       mul!(ν, Z, a, -1.0, 1.0)
\end{verbatim}
  \item   $M_t = Z P_{t|t-1}$
\begin{verbatim}
       mul!(M, Z, P)
\end{verbatim}
    \item $F_t = M_tZ' + H$
\begin{verbatim}
       copy!(F, H)
       mul!(F, M, Z', 1.0, 1.0) 
\end{verbatim}
    \item $iF\nu_t = F_t^{-1}\nu_t$
\begin{verbatim}
       potrf!('U', F)
       potrs!('U', F, ν)
\end{verbatim}
    \item $a_{t|t} = a_{t|t-1} + M_t' iF\nu_t$
\begin{verbatim}
       copy(a1, a)
       mul!(a1, M', ν, 1.0, 1.0)
\end{verbatim}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Kalman  filter recursion in Julia (II)}
  \begin{enumerate}
    \setcounter{enumi}{5}
    \item $iFM_t = F_t^{-1}M_t$
\begin{verbatim}
       potrs!('U', F, M)
\end{verbatim}
    \item $P_{t|t} = P_{t|t-1} - M_t' iFM_t$
\begin{verbatim}
       copy!(P1, P)
       mul!(P, M', iFM, -1.0, 1.0)
\end{verbatim}
    \item $a_{t+1|t} = T a_{t|t}$
\begin{verbatim}
       mul!(a,T,a1)
\end{verbatim}
    \item $QQ = RQR'$
\begin{verbatim}
       mul!(R1,R,Q)
       mul!(QQ,R1,Q)
\end{verbatim}
    \item $P_{t+1|t} = T P_{t|t} T' + QQ$
\begin{verbatim}
       copy!(P1, P)
       mul!(P2, T, P1)
       copy!(P, QQ)
       mul!(P, T, P2, 1.0, 1.0)
\end{verbatim}
  \end{enumerate}
\end{frame}
  
\begin{frame}[fragile]
  \frametitle{Log likelihood}
  \[
    LIK = \sum_{t=1}^{Nobs} -\frac{1}{2}\left(n\ln 2\pi + \ln \left|F_t\right| +
         \nu_t' F_t^{-1} \nu_t\right)
  \]
  Julia implementation
\begin{verbatim}
        lik[t] = log(det_from_cholesky(F)) 
                         + LinearAlgebra.dot(ν, iFν)

        lik_cst = nobs*n*log(2*pi)
        LIK = -0.5*(lik_cst + sum(lik))
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{KalmanLikelihoodWs (I)}
\begin{verbatim}
struct KalmanLikelihoodWs{T, U}
  R1::Matrix{T}
  QQ::Matrix{T}
  ν::Vector{T}
  M::Matrix{T}
  F::Matrix{T}
  iFν::Vector{T}
  iFM::Matrix{T}  
  a1::Vector{T}
  P1::Matrix{T}
  P2::Matrix{T}
  lik::Vector{T}
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{KalmanLikelihoodWs (II)}
\begin{verbatim}
  function KalmanLikelihoodWs(ny::U, ns::U, np::U, nobs::U)
    R1::Matrix{T}(undef, ns, np)
    QQ::Matrix{T}(undef, ns, ns)
    ν::Vector{T}(undef, ny)
    M::Matrix{T}(undef, ny, ns)
    F::Matrix{T}(undef, ny, ny)
    iFν::Vector{T}(undef, ny)
    iFM::Matrix{T}(undef, ny, ns)
    a1::Vector{T}(undef, ns)
    P1::Matrix{T}(undef, ns, ns)
    P2::Matrix{T}(undef, ns, ns)
    lik::Vector{T}(undef, nobs)
    new(R1, QQ, ν, M, F, iFν, IFM, a1, P1, P2, lik)
  end
end 
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Exploiting multiple dispatch}
  \begin{itemize}
    \item When $Z$ is a selection matrix, extracting rows and columns
      is faster than mutliplying
    \item It is sufficient to replace matrix $Z$ by a vector of
      integers and specialize 
      \begin{enumerate}
      \item function get\_nu() that computes
        \[
          \nu_t = y_t - Za_{t|t-1}
        \]
      \item function get\_F() that computes
        \[
          F_t = Z P_{t|t-1} Z' + H
        \]
        and stores $ZP = Z\cdot P$ 
        \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Performance}
  \begin{center}
    \begin{tabular{rrrr}}
      & KalmanFiltertools & SsfPack & Dynare Matlab \\
      nile & & 0.00098 & 0.00043\\
      nile10 & & 0.06492 & 0.0030\\
      nile100 & & 68.641 & 0.0795\\
    \end{tabular{rrrr}}
  \end{center}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Parallel computing}
  \begin{itemize}
  \item Kalman filter algorithm is serial by nature: no scope for
    parallel computing
  \item Matrix multiplication can be computed in parallel
  \item   On a computer with several cores, OpenBlas may split matrix multiplication in too
  many threads
  \item Example for a model with 60 state variables and 12 observables\\
    \begin{tabular}{ll}
      1 threads &  27.171 ms\\
      2 threads &  26.905 ms\\
      3 threads &  26.151 ms\\
      4 threads &  33.104 ms\\
      5 threads &  32.258 ms\\
      6 threads &  38.128 ms\\
      7 threads &  34.826 ms\\
      8 threads &  46.085 ms
    \end{tabular}
  \item Use \verb+BLAS.set_num_threads()+
  \item If there is scope to run several Kalman filters in parallel,
    allocate only one thread to OpenBlas
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Additional functionalities}
  \begin{itemize}
  \item $Z$ as a selection matrix
  \item Missing observations
  \item Monitoring for steady state of the filter
  \item State filtering, smoothing and forecasting
  \item Unobserved variables and innovations smoothing
  \item Chandrasekhar-Herbst fast recursion
  \item Durbin Koopman diffuse filter for non-stationary models
  \item Time varying state space models
  \item Derivative of log likelihood with respect with elements of
    system matrices
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Package available on Gitlab}
  \url{https://gitlab.com/MichelJuillard/KalmanFilterTools}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Conclusion}
  \begin{itemize}
  \item For critical code, optimizing Julia code brings about
    significant benefits
  \item Pre-allocate workspace
  \item Use low level in place functions
  \item Write small kernels
  \item Try to reuse past computations
  \item Find the best setting for \verb+BLAS.set_num_threads()+
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Credits}
Sunset  Image by <a
href="https://pixabay.com/users/magicmicka-8023989/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4359904">magicmicka</a>
from <a
href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4359904">Pixabay</a>
Falouk Image by <a href="https://pixabay.com/users/juhasztamas08-8561159/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3804407">Tamás Juhász</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3804407">Pixabay</a>
Kalman: Par Greuel, Gert-Martin — https://opc.mfo.de/detail?photo_id=11513, CC BY-SA 2.0 de, https://commons.wikimedia.org/w/index.php?curid=18069950


\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
