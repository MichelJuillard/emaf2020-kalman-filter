#include <oxstd.h>
#include <packages/ssfpack/ssfpack.h>

static decl s_mYt, s_mPhi, s_mOmega, s_mSigma;

main()
{
    decl dlik, dvar, i, t0, n, Y, niter;

    // load Nile data and transpose
    s_mYt = loadmat("../Nile.mat")';
    n = 100;
    Y = ones(n, 1) * s_mYt;
    // set state space definition matrix
    s_mPhi = unit(n) | unit(n);
    s_mOmega = 1469*unit(n) ~ zeros(n, n) |  zeros(n, n) ~ 15099*unit(n);
    s_mSigma = 1e7*unit(n) | zeros(1, n);
    niter = 1e2;
    t0 = timer();
    for (i = 1; i <= niter; ++i)
    {
        SsfLik(&dlik, &dvar, Y, s_mPhi, s_mOmega, s_mSigma);
    }
    println((timer() - t0)/niter);
}
