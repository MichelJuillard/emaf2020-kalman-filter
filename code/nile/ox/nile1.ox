#include <oxstd.h>
#include <packages/ssfpack/ssfpack.h>

static decl s_mYt, s_mPhi, s_mOmega, s_mSigma;

main()
{
    decl dlik, dvar, i, t0;

    // load Nile data and transpose
    s_mYt = loadmat("../Nile.mat")';
    // set state space definition matrix
    s_mPhi = < 1; 1 >;
    s_mOmega = < 1469, 0; 0, 15099>;
    s_mSigma = <1e7; 0>;
    t0 = timer();
    for (i = 1; i <= 1e6; ++i)
    {
        SsfLik(&dlik, &dvar, s_mYt, s_mPhi, s_mOmega, s_mSigma);
    }
    println((timer() - t0)/1e6);
}
