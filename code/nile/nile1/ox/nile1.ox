#include <oxstd.h>
#include <oxdraw.h>
#import <maximize>
#include <packages/ssfpack/ssfpack.h>

static decl s_mYt, s_mPhi, s_mOmega, s_mSigma, s_dVar;
static decl s_mSsf;

Likelihood(const vP, const pdLik, const pvSco, const pmHes)
{	// arguments dictated by MaxBFGS()
    decl dvar, msco, ct = columns(s_mYt);
	s_mSsf[0:1][1] = exp(vP);	// update ssf definition
	GetSsfStsm(s_mSsf, &s_mPhi, &s_mOmega, &s_mSigma);
    if (pvSco) {				// yes: score requested
		SsfLikSco(pdLik, &s_dVar, &msco,
			s_mYt, s_mPhi, s_mOmega, s_mSigma);
        pvSco[0] = (diagonal(s_mOmega) .* diagonal(msco))'/ct;
    } else						// no: num score required
        SsfLik(pdLik, &s_dVar,
			s_mYt, s_mPhi, s_mOmega, s_mSigma);
    pdLik[0] /= ct;				// log-likelihood scaled by n
    return 1;					// 1 = success, 0 failure
}

main()
{
    decl vp, ir, dlik, dvar, i, t0;

    // load Nile data and transpose
    s_mYt = loadmat("../../Nile.mat")';
    // set state space definition matrix
	s_mSsf = <CMP_LEVEL, 0.5, 0, 0;
			  CMP_IRREG, 1.0, 0, 0>;
    println(s_mSsf);			  
    // scale initial estimates for better starting values
    t0 = timer();
    for (i = 1; i <= 1; ++i)
    {
        vp = log(<0.5;1>);			// starting values log(sigma)
	Likelihood(vp, &dlik, 0, 0);// evaluate lik at start val
    }
    println(timespan(t0));
}
