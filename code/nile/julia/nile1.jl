using BenchmarkTools
using KalmanFilterTools

data = open(f->read(f, String), "../../Nile.mat")
data1 = split(data, "\r\n")
y=map(s->parse(Float64, s), data1[2:101])
y = convert(Vector{Union{Missing, Float64}}, y)
y = Matrix(reshape(y, 1, length(y)))
Z = hcat(1.0)
T = hcat(1.0)
R = hcat(1.0)
H = hcat(15099.0)
Q = hcat(1469.1)
a = [0.0]
P = hcat(1.0e7)

ws = KalmanLikelihoodWs(1, 1, 1, 100)
loglik = kalman_likelihood(y, Z, H, T, R, Q, a, P, 1, 100, 0, ws) 

function f(y, Z, H, T, R, Q, a, P, start, last, presample, ws, niter)
    for i = 1:niter
        kalman_likelihood(y, Z, H, T, R, Q, a, P, start, last, presample, ws) 
    end
end

@time f(y, Z, H, T, R, Q, a, P, 1, 100, 0, ws, 1000)
