using BenchmarkTools
using KalmanFilterTools
using LinearAlgebra

data = open(f->read(f, String), "../Nile.mat")
data1 = split(data, "\r\n")
y=map(s->parse(Float64, s), data1[2:101])
y = convert(Vector{Union{Missing, Float64}}, y)
y = Matrix(reshape(y, 1, length(y)))
n = 10
y = repeat(y, outer=n)
Z = Matrix(1.0*I(n))
T = Matrix(1.0*I(n))
R = Matrix(1.0*I(n))
H = Matrix(15099.0*I(n))
Q = Matrix(1469.1*I(n))
a = zeros(n)
P = Matrix(1.0e7*I(n))

ws = KalmanLikelihoodWs(n, n, n, 100)
loglik = kalman_likelihood(y, Z, H, T, R, Q, a, P, 1, 100, 0, ws) 

function f(y, Z, H, T, R, Q, a, P, start, last, presample, ws, niter)
    for i = 1:niter
        kalman_likelihood(y, Z, H, T, R, Q, a, P, start, last, presample, ws) 
    end
end

@time f(y, Z, H, T, R, Q, a, P, 1, 100, 0, ws, 1000)
