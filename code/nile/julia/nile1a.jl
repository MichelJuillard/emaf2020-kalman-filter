using BenchmarkTools
using KalmanFilterTools
using Plots

data = open(f->read(f, String), "../Nile.mat")
data1 = split(data, "\r\n")
y=map(s->parse(Float64, s), data1[2:101])
y = convert(Vector{Union{Missing, Float64}}, y)
y = Matrix(reshape(y, 1, length(y)))
Z = hcat(1.0)
T = hcat(1.0)
R = hcat(1.0)
H = hcat(15099.0)
Q = hcat(1469.1)
a0 = [0.0]
P0 = hcat(1.0e7)

ws = KalmanLikelihoodWs(1, 1, 1, 100)
a = copy(a0)
P = copy(P0)
loglik = kalman_likelihood(y, Z, H, T, R, Q, a, P, 1, 100, 0, ws) 

function f(y, Z, H, T, R, Q, a, P, start, last, presample, ws, niter)
    for i = 1:niter
        a = copy(a0)
        P = copy(P0)
        kalman_likelihood_monitored(y, Z, H, T, R, Q, a, P, start, last, presample, ws) 
    end
end

timings = zeros(100)
global it1
for it in 1:100
    global it1 = it
    local a = copy(a0)
    local P = copy(P0)
    res = @benchmark f(y, Z, H, T, R, Q, a, P, 1, it1, 0, ws, 1000)
    @show it, minimum(res).time
    timings[it] = minimum(res).time
end

plot(1:100, timings)
