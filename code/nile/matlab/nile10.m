fh = fopen('../Nile.mat');
fgetl(fh);
Y = cell2mat(textscan(fh, '%f'))';

n = 10;
Y = repmat(Y, n, 1);
Z = eye(n);
T = eye(n);
R = eye(n);
H = 15099.0*eye(n);
Q = 1469.1*eye(n);
a = zeros(n,1);
P = 1.0e7*eye(n);

tt = tic;
niter = 1000;
for i=1:niter
 llik = kalman_filter(Y, 1, 100, a, P, 1e-10, 1e-10, false , 0, T, Q, R, H, Z, n, n, n, true, ...
                      false, 0, false, [], [], [], [], [], [], [], [], []);
end
toc(tt)/niter
