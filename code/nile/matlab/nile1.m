addpath ../../../matlab/

fh = fopen('../Nile.mat');
fgetl(fh);
Y = cell2mat(textscan(fh, '%f'))';

Z = 1.0;
T = 1.0;
R = 1.0;
H = 15099.0;
Q = 1469.1;
a = 0.0;
P = 1.0e7;

tt = tic;
niter = 1000;
for i=1:niter
 llik = kalman_filter(Y, 1, 100, a, P, 1e-10, 1e-10, false , 0, T, Q, R, H, Z, 1, 1, 1, false, ...
                      false, 0, false, [], [], [], [], [], [], [], [], []);
end
toc(tt)/niter
