fh = fopen('../Nile.mat');
fgetl(fh);
Y = cell2mat(textscan(fh, '%f'))';

Z = 1.0;
T = 1.0;
R = 1.0;
H = 15099.0;
Q = 1469.1;
a = 0.0;
P = 1.0e7;

timing = [];
for last = 1:100
 tt = tic;
 for i=1:1000
   llik = kalman_filter(Y, 1, last, a, P, 0, 0, false , 0, T, Q, R, H, Z, 1, 1, 1, false, ...
                        false, 0, false, [], [], [], [], [], [], [], [], []);
  end
 timing(end+1) = toc(tt);
end

figure; plot((1:100), timing)
